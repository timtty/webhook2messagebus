package org.ttremblay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Webhook2msgbusApplication {

    public static void main(String[] args) {
        SpringApplication.run(Webhook2msgbusApplication.class, args);
    }

}
