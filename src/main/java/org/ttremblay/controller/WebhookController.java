package org.ttremblay.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.*;
import org.ttremblay.http.WebhookResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
public class WebhookController {
    final Logger logger = LoggerFactory.getLogger(WebhookController.class);
    final RabbitTemplate rabbitTemplate;

    public WebhookController(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @RequestMapping(value = "/{appId}", method = RequestMethod.POST)
    public WebhookResponse entryPoint(HttpServletRequest servletRequest,
                                      @RequestHeader Map<String, String> headers,
                                      @PathVariable("appId") String appId,
                                      @RequestBody() String body) {

        String contentType = headers.get("content-type");
        String userAgent = headers.get("user-agent");
        String remoteIp = servletRequest.getRemoteAddr();

        String logMsg = "Application ID: " + appId + ", Host: " + remoteIp +
                ", UA: " + userAgent + ", Content: " + contentType +
                ", Size: " + body.length() + " byte(s)";
        logger.info(logMsg);

        RabbitAdmin rabbitAdmin = new RabbitAdmin(rabbitTemplate);
        rabbitAdmin.declareExchange(new FanoutExchange(appId));
        rabbitTemplate.convertAndSend(appId, "", body);

        return new WebhookResponse(true, "Accepted " + body.length() + " byte(s)");
    }
}
