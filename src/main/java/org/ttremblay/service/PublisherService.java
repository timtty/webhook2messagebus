package org.ttremblay.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.QueueInformation;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class PublisherService {
    final Logger logger = LoggerFactory.getLogger(PublisherService.class);
    final RabbitTemplate rabbitTemplate;

    public PublisherService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void publish(String applicationId, String message) {
        RabbitAdmin rabbitAdmin = new RabbitAdmin(rabbitTemplate);

        QueueInformation info = rabbitAdmin.getQueueInfo(applicationId);
        rabbitAdmin.declareExchange(new FanoutExchange(applicationId));

        rabbitTemplate.convertAndSend(applicationId, "", message);
    }
}
