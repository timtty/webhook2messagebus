package org.ttremblay.http;

import org.springframework.web.bind.annotation.ResponseBody;

@ResponseBody
public class WebhookResponse {
    Boolean status;
    String detail;

    public WebhookResponse(Boolean status, String detail) {
        this.status = status;
        this.detail = detail;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getDetail() {
        return detail;
    }
}
