package org.ttremblay.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Value("${app.http_auth.username}")
    String authUsername;

    @Value("${app.http_auth.password}")
    String clearPassword;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        String hashPassword = encoder.encode(clearPassword);

        auth.inMemoryAuthentication()
                .passwordEncoder(encoder)
                .withUser(authUsername)
                .password(hashPassword)
                .authorities("_A_")
                ;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests().anyRequest()
                .authenticated().and().httpBasic()
                ;
    }
}
